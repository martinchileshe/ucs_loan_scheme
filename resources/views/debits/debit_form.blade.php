@extends('layouts.main')
@section('header','Debit')
@section('descript','Issue new debit to member')
@section('data', 'Create')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Enter debit details
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" method="POST" action="{{ url('/debit/receipt/create') }}">
                        <div class="col-lg-6">

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('man_number') ? ' has-error' : '' }} form-inline">
                                <label for="man_number" class="col-md-4 control-label">Man number</label>

                                <input id="man_number" type="number" class="form-control" name="man_number" value="{{ old('man_number') }}">

                                @if ($errors->has('man_number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('man_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">

                            <fieldset>

                                    <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }} input-group">
                                        <label for="amount" class="col-md-4 control-label">Amount</label>
                                        <span class="input-group-addon">KR</span>
                                        <input name="amount" type="number" class="form-control" value="{{ old('amount') }}">
                                        <span class="input-group-addon">.00</span>

                                    @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                    @endif
                                    </div>

                            </fieldset>
                            <div class="form-group col-lg-offset-5">
                                <button type="submit" class="btn btn-default">Submit Button</button>

                                <button type="reset" class="btn btn-default ">Reset Button</button>
                            </div>

                        </div>
                    </form>
            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
        @endsection