@extends('layouts.main')
    @section('descript','Viewing all members of staff')
        @section('header', 'Staff')
        @section('data', 'All')
            @section('content')

                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Members
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover responsive-utilities" data-toggle="table" id="dataTables-example" data-show-refresh="false"
                                   data-show-toggle="true" data-show-columns="true" data-search="true"
                                   data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name"
                                   data-sort-order="desc" style="font-size: small">
                                <thead>
                                <tr>
                                    <th>Man number</th>
                                    <th>first name</th>
                                    <th>Last name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Edit/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($staff as $staff)
                                <tr class="odd gradeX">
                                    <td>{{$staff->man_number}}</td>
                                    <td>{{$staff->f_name}}</td>
                                    <td>{{$staff->l_name}}</td>
                                    <td class="center">{{$staff->email}}</td>
                                    <td class="center">{{$staff->phone}}</td>
                                    <td class="center">{{$staff->physical_address}}</td>
                                   <td class="center">
                                       <div class="btn-group">
                                           <a href="{{url('/staff/credentials/update/'.$staff->man_number)}}" class="btn btn-sm btn-link">Edit</a>
                                           <!--<a onclick="delete_user('$staff->first_name}}', '$staff->man_number}}')"
                                              class="btn btn-sm btn-link">Delete</a>-->
                                           <!--<a class="btn btn-default btn-xs" type="button" title="edit">
                                               <i href="url('/edit_user/'.$staff->man_number)}}" class="glyphicon glyphicon glyphicon-edit"></i>
                                           </a>-->
                                           <button class="btn btn-default btn-xs" onclick="delete_user('{{$staff->f_name}}', '{{$staff->man_number}}')" type="button" name="toggle" title="delete">
                                               <i class="glyphicon glyphicon glyphicon-trash"></i>
                                           </button>

                                       </div>
                                   </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
                </div>
                </div>

                <script>
                    $(document).ready(function () {
                        $('#dataTables-example').dataTable();
                    });
                </script>

                <script>
                    function delete_user(user, man) {
                        var xhttp;
                        if (window.XMLHttpRequest) {
                            xhttp = new XMLHttpRequest();
                        } else {
                            // code for IE6, IE5
                            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        if (confirm("Are you sure you want to delete " + user + "?")) {
                            xhttp.open("GET", "{{url('/staff/credentials/delete')}}/" + man, false);
                            xhttp.send();
                            alert(user + " has been deleted!");
                            location.reload();
                        }

                    }
                </script>

            @endsection