﻿@extends('layouts.main')
@section('header','Add Staff')
@section('data', 'New')
@section('descript','Add new member of staff to the system')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="{{ url('/staff/new/store') }}">
                            <div class="col-lg-6">

                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('man_number') ? ' has-error' : '' }}">
                                    <label for="man_number" class="col-md-4 control-label">Man number</label>

                                        <input id="man_number" type="number" class="form-control" name="man_number" value="{{ old('man_number') }}">

                                        @if ($errors->has('man_number'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('man_number') }}</strong>
                                    </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('f_name') ? ' has-error' : '' }}">
                                    <label for="f_name" class="col-md-4 control-label">First Name</label>
                                        <input id="f_name" type="text" class="form-control" name="f_name" value="{{ old('f_name') }}">

                                        @if ($errors->has('f_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('f_name') }}</strong>
                                    </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('l_name') ? ' has-error' : '' }}">
                                    <label for="l_name" class="col-md-4 control-label">Last Name</label>

                                        <input id="l_name" type="text" class="form-control" name="l_name" value="{{ old('l_name') }}">

                                        @if ($errors->has('l_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('l_name') }}</strong>
                                    </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('physical_address') ? ' has-error' : '' }} ">
                                    <label for="physical_address" class="col-md-4 control-label">Physical Address</label>

                                    <input type="text" class="form-control" name="physical_address" value="{{old('physical_address')}}">

                                    @if ($errors->has('physical_address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('physical_address') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-4 control-label">Contact number</label>

                                    <input id="phone" type="tel" class="form-control" name="phone" value="{{old('phone')}}">

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            <div class="col-lg-6">

                                <fieldset>
                                    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                        <label for="position" class="col-md-4 control-label">Position</label>

                                            <select id="disabledInput" type="text" class="form-control" name="position">
                                                <option value="{{old('position')}}">{{old('position')}}</option>
                                                <option value="Academic staff">Academic staff</option>
                                                <option value="Support staff">Support staff</option>
                                                <option value="Head of Department">Head of Department</option>
                                            </select>

                                            @if ($errors->has('position'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                            @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <input id="disableSelect" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                    </div>
                                    </fieldset>
                                    <div class="form-group col-lg-offset-5">
                                    <button type="submit" class="btn btn-default">Submit Button</button>

                                    <button type="reset" class="btn btn-default ">Reset Button</button>
                                    </div>
                            </div>
                        </form>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                @endsection