<!DOCTYPE html>
<html lang="en">
<html xmlns="{{URL::asset('http://www.w3.org/1999/xhtml')}}">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title')</title>
    <!-- Bootstrap Styles-->
    <link href="{{URL::asset('../assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="{{URL::asset('../assets/css/font-awesome.css')}}" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="{{URL::asset('../assets/css/custom-styles-guest.css')}}" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='{{URL::asset('http://fonts.googleapis.com/css?family=Open+Sans')}}' rel='stylesheet' type='text/css' />
</head>
<body>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"><strong>bluebox</strong></a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>

    @yield('content')

</div>
<footer >
    <p>
    <div class="footer">&copy;All right reserved  {{\Carbon\Carbon::today()->toDateString()}}
    </div>
    </p>
</footer>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="{{URL::asset('../assets/js/jquery-1.10.2.js')}}"></script>
<!-- Bootstrap Js -->
<script src="{{URL::asset('../assets/js/bootstrap.min.js')}}"></script>
<!-- Metis Menu Js -->
<script src="{{URL::asset('../assets/js/jquery.metisMenu.js')}}"></script>
<!-- Custom Js -->
<script src="{{URL::asset('../assets/js/custom-scripts.js')}}"></script>
@yield('scripts')


</body>
</html>
