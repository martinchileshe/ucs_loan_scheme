@extends('layouts.main')
@section('descript','Overview of current financial records')
@section('header', 'Home')
@section('content')
        <!-- Morris Chart Js -->
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="panel panel-primary text-center no-boder blue">
                    <div class="panel-left pull-left blue">
                        <i class="fa fa-money fa-5x"></i>

                    </div>
                    <div class="panel-right">
                        <h3>K {{$main->available_cash}}</h3>
                        <strong>Current account balance</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="panel panel-primary text-center no-boder blue">
                    <div class="panel-left pull-left blue">
                        <i class="fa fa-folder-open fa-5x"></i>
                    </div>

                    <div class="panel-right">
                        <h3>K {{$main->initial_balance}}</h3>
                        <strong>Start amount</strong>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="panel panel-primary text-center no-boder blue">
                    <div class="panel-left pull-left blue">
                        <i class="fa fa fa-backward fa-5x"></i>

                    </div>
                    <div class="panel-right">
                        <h3>K {{$main->credits_pending}}</h3>
                        <strong>Total amount to be paid back</strong>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="panel panel-primary text-center no-boder blue">
                    <div class="panel-left pull-left blue">
                        <i class="fa fa-level-up fa-5x"></i>

                    </div>
                    <div class="panel-right">
                        <h3>{{$main->pec_interest_rate}} %</h3>
                        <strong>Current interest rate</strong>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Recent transactions
                    </div>
                    <div class="panel-body">
                        <div class="list-group">

                            @foreach($debit_receipts as $receipt)

                            <a href="#" class="list-group-item">
                                <span class="badge">K {{$receipt->amount_debited}}</span>
                                <i class="fa fa-fw fa-comment"></i> {{$receipt->owner->f_name}} Debited
                            </a>
                            @endforeach
                        </div>
                        <div class="text-right">
                            <a href="#">More Tasks <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>

            </div>

        <div class="col-md-8 col-sm-12 col-xs-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Staff with debits
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Man No.</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Amount</th>
                                <th>Since</th>
                            </tr>
                            </thead>
                            <tbody>

                               @foreach($debits as $debit)
                                   <tr>
                                <td>{{$debit->man_number}}</td>
                                <td>{{$debit->user->f_name}}</td>
                                <td>{{$debit->user->l_name}}</td>
                                <td>{{$debit->amount_due}}</td>
                                <td>{{\Carbon\Carbon::parse($debit->created_at)->diffForHumans()}}</td>
                                   </tr>
                                    @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
