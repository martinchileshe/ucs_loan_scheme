<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffDebitReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_receipt', function (Blueprint $table) {
            $table->string('man_number');
            $table->double('amount_debited', 8,2)->default(0.00);
            $table->timestamps();
            $table->double('pec_interest_rate',3,1)->nullable();
            $table->string('issuer',10);
            $table->string('debit_id')->unique();
        });

        Schema::table('debit_receipt', function (Blueprint $table) {
            $table->foreign('man_number')->references('man_number')->on('staff')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pec_interest_rate')->references('pec_interest_rate')->on('main_account')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('debit_receipt');
    }
}
