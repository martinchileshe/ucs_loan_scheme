<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffCreditReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_receipt', function (Blueprint $table) {
            $table->string('credit_id');
            $table->string('man_number');
            $table->double('amount_credited', 8,2)->default(0.00);
            $table->timestamps();
            $table->string('paid_to');
        });

        Schema::table('credit_receipt', function (Blueprint $table) {
            $table->foreign('man_number')->references('man_number')->on('staff')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('credit_receipt');
    }
}
