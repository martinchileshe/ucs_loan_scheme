<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffWithDebits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_with_debits', function (Blueprint $table) {
            $table->string('man_number',10);
            $table->double('amount_debited', 8,2)->default(0.00);
            $table->double('amount_due', 8,2)->default(0.00);
            $table->boolean('overdue')->default(false);
            $table->timestamps();
            $table->string('debit_id');

            $table->foreign('man_number')->references('man_number')->on('staff')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('debit_id')->references('debit_id')->on('debit_receipt')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff_with_debits');
    }
}
