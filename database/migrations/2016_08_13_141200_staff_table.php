<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->string('man_number', 10)->unique()->primary();
            $table->string('f_name', 60);
            $table->string('l_name', 60);
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('physical_address',60)->nullable();
            $table->enum('position', ['Head of Department', 'Academic staff', 'Support staff']);
            $table->string('password')->nullable();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff');
    }

}
