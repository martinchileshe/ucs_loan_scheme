<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MainAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_account', function (Blueprint $table) {
                //
                    $table->increments('id');
                    $table->double('initial_balance', 8, 2)->default(300.0);
                    $table->double('minimum_balance', 8,2)->default(300.0);
                    $table->double('available_cash',8,2);
                    $table->double('pec_interest_rate',3,1)->default(17.0)->unique();
                    $table->double('current_debits_total',8,2)->default(0.00);
                    $table->double('credits_pending',8,2)->default(0.00);
                    $table->timestamps();
                });

            }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main_account');
    }
}
