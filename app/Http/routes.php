<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Route::auth();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/view/staff/all', 'staff_controller@all');
    Route::get('/staff/new/add', 'staff_controller@staff_add_form');
    Route::post('/staff/new/store', 'staff_controller@store');
    Route::get('/staff/credentials/update/{id}', 'staff_controller@update');
    Route::get('/staff/credentials/delete/{id}', 'staff_controller@delete');
    Route::post('/staff/credentials/save', 'staff_controller@save');
    Route::get('/staff/debit/remind', 'staff_controller@remind');
    Route::get('/staff/debit/overdue/request', 'staff_controller@request');
    Route::get('/debit/main/account/form', 'debit_receipt_controller@debit_form');
    Route::post('/debit/receipt/create', 'debit_receipt_controller@create');
    Route::get('/credit/main/account/form', 'credit_receipt_controller@credit_form');
    Route::post('/credit/receipt/create', 'credit_receipt_controller@create');
    Route::get('/main/account/show', 'main_account_controller@show');
    Route::get('/staff/debit/remind', 'staff_with_debits@remind');
    Route::get('/home', 'HomeController@index');
    Route::get('/calculate', function(){

        $old = 900.0;
        $old = ($old-(1051-((16.7*900)/100)));
        return $old;
    });

});

