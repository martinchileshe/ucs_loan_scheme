<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class staff_request extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
                'f_name' => 'required|max:60',
                'l_name' => 'required|max:60',
                'man_number' => 'required|unique:staff|max:10',
                'email' => 'required|email|max:255|unique:staff',
                'position' => 'required',
                'phone' =>'sometimes|required',
                'physical_address' =>'sometimes|required'
        ];
    }
}
