<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class new_debit_validation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'amount' => 'required|max:999999|min:3|isLimit|isMultiple|max:8',
                'man_number' => 'required|exists:staff|max:10',
        ];
    }
}
