<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class update_staff extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [

            'f_name' => 'sometimes|required|max:60',
            'l_name' => 'sometimes|required|max:60',
            'man_number' => 'sometimes|required|unique:staff|max:10',
            'email' => 'sometimes|required|email|max:255',
            'position' => 'sometimes|sometimes|required',
            'phone' =>'sometimes|required',
            'physical_address' =>'sometimes|required'

        ];
    }
}
