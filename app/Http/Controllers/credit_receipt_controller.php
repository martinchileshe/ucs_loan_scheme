<?php

namespace App\Http\Controllers;

use App\Credit;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Main_account;
use App\Staff;
use Illuminate\Support\Facades\Auth;

class credit_receipt_controller extends Controller
{
    //returns form for credits into the main account
    public function credit_form(){

        return view('credits.credit_form');
    }


    //create and save the credit receipt into the credit table
    public function create(Request $request){

        $this->validate($request, ['amount'=>'required|numeric', 'man_number'=> 'required|exists:staff']);

        $main_account = Main_account::find(1)->first();

        //get the debit instance model for this man number from the staff with debits table
        $debit = Staff::findOrfail($request->man_number)->debit;

        //generates unique id
        $id = uniqid();

        //create and save credit receipt
        Credit::create(['credit_id' => $id, 'man_number' => $request->man_number, 'amount_credited'=>$request->amount,
            'paid_to' =>Auth::user()->man_number]);


        //deduct amount due if this user is still owing and delete user from the record of
        // staff with debits if the resulting due amount is less than or equal to zeroe
        if($debit != null){

            $debit->amount_due = $debit->amount_due - $request->amount;
            $debit->save();

            if($debit->amount_due<=0){
                $debit->delete();
            }


        }

        //increment available cash
        $main_account->available_cash = $main_account->available_cash + $request->amount;

        //decrement total debits
        $main_account->current_debits_total = ($main_account->current_debits_total - ($request->amount-(($main_account->pec_interest_rate * $request->amount)/100)));

        //decrement total pending credits
        $main_account->credits_pending = $main_account->credits_pending - $request->amount;
        $main_account->save();

        //redirect to home page
        return Redirect::action('HomeController@index')
;    }
}
