<?php

namespace App\Http\Controllers;

use Doctrine\DBAL\Schema\View;
use Illuminate\Http\Request;
use App\Staff;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class staff_controller extends Controller
{
    //

    //fetches and displays all registered users in the system
    public function all(){

        $staff = Staff::all();

        return view('staff.view_all')->with('staff',$staff);

    }





    //returns a from for registartion of a new user into the system
    public function staff_add_form(){
        return view('staff.add_staff_form');
    }





    //stores the submitted credentials into the staff table
    public function store(Requests\staff_request $request){

        Staff::create(['man_number' => $request->man_number,'f_name' => $request->f_name,
            'l_name'=>$request->l_name, 'phone'=>$request->phone,'physical_address' => $request->physical_address, 'email'=>$request->email]);

        return Redirect::action('HomeController@index');
    }





    //returns form for updating user credentials
    public function update($id){

        $staff = Staff::findOrfail($id);
        return view('staff.update')->with('staff', $staff);
    }






    //save updated staff credentials to the staff table
    public function save(Requests\update_staff $request,$id){

        if($request->all() != null){

           $staff = Staff::findOrfail($id);

                $staff->fill(['man_number' => $request->man_number, 'f_name' => $request->f_name,
                    'l_name' => $request->l_name, 'phone' => $request->phone, 'physical_address' => $request->physical_address, 'email' => $request->email]);

                $staff->save();

        }

        return Redirect::action('staff_controller@all');

    }






    //deletes user from staff table
    public function delete($id){

        $staff = Staff::findOrFail($id);
        $staff->delete();

    }




    //sends a reminder to user about the debits
    public function remind(Request $request){


    }




    //sends a request for over due payments to user
    public function request(Request $request){

    }
}
