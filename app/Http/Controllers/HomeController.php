<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Staff_with_debits;
use Illuminate\Http\Request;
use App\Main_account;
use App\Debit;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main = Main_account::find(1)->first();

        $debits = Staff_with_debits::all();

        $debit_receipts = Debit::all();

        if($debit_receipts != null)
        return view('home')->with(['main'=>$main, 'debit_receipts' => $debit_receipts, 'debits' => $debits]);

        return view('home')->with(['main'=>$main, 'debits' => $debits]);

    }
}
