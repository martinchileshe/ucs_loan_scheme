<?php

namespace App\Http\Controllers;

use App\Debit;
use App\Staff_with_debits;
use Illuminate\Http\Request;

use App\Http\Requests;
use app\Main_account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;

class debit_receipt_controller extends Controller
{
    //returns a from issuing a debit to a member of staff
    public function debit_form(){

        return view('debits.debit_form');
    }


    //creates receipt and save debit details to debit table
    public function create(Requests\new_debit_validation $request){

        //ensure the user is not owing before borrowing again
        $this->validate($request, ['man_number'=>'required|unique:staff_with_debits'], ['unique' => 'This member still has an unpaid balance']);

        $main_account = Main_account::find(1)->first();
        $id = uniqid();//generates unique id

       Debit::create(['debit_id' => $id, 'man_number' => $request->man_number, 'amount_debited'=>$request->amount,
            'pec_interest_rate' => $main_account->pec_interest_rate, 'issuer' =>Auth::user()->man_number]);

        //calculate amount due using interest
        $amount_due = $request->amount + (17*$request->amount)/100;

        //deduct available cash and increment the total debited amount
        $main_account->available_cash = $main_account->available_cash - $request->amount;
        $main_account->current_debits_total = $main_account->current_debits_total+$request->amount;

        //increment the total credits pending and save
        $main_account->credits_pending = $main_account->credits_pending+$amount_due;
        $main_account->save();


        //create entry into staff with debits table
        Staff_with_debits::create(['man_number' => $request->man_number,'amount_debited'=>$request->amount,
            'amount_due'=> $amount_due, 'debit_id' =>$id]);

        return Redirect::action('HomeController@index');

    }
}
