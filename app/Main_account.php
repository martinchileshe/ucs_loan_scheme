<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Main_account extends Model
{
    //
    protected $table = 'main_account';
    protected $fillable = ['initial_balance','minimum_balance','credits_pending', 'available_cash',
        'interest_rate', 'current_debts_total'];
}
