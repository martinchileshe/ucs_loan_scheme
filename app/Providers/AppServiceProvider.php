<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Main_account;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('isMultiple', 'App\Validators\debit_validator@isMultiple');
        Validator::extend('isLimit', 'App\Validators\debit_validator@isLimit');

        Validator::replacer('isLimit', function($message, $attribute, $rule, $parameters) {
            return str_replace('validation.is_limit', 'This amount exceeds the debit limit', 'validation.is_limit');
        });

        Validator::replacer('isMultiple', function($message, $attribute, $rule, $parameters) {
            return str_replace('validation.is_multiple', 'Amount must be a multiple of the account minimum balance','validation.is_multiple');
        });



    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
