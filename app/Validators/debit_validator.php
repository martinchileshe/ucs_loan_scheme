<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/16/16
 * Time: 3:04 PM
 */

namespace App\Validators;

use App\Main_account;


class debit_validator

{

    public $min;
    public $available;

    //constructors initialises required values to the coresponding database values

    public function __construct(){

         $main = Main_account::all();

        foreach ($main as $record) {
            $this->min = $record->minimum_balance;
            $this->available = $record->available_cash;
        }
    }

    //this valiodation ensures that the amount to be borrowed is a multiple of the account minimum balance

    public function isMultiple($field, $value, $parameters){

            //$value = number_format($value,2);

            return (($value % $this->min) == 0);

    }

    //this validator ensures that the amount you want to borrow will not leave the account with less than that the
    //defined minimum balance

    public function isLimit($field, $value, $parameters){

        $value = number_format($value,2);

        return ($value <= ($this->available-$this->min));
    }
}