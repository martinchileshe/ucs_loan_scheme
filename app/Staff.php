<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    public $timestamps = false;
    protected $increments = false;
    protected $primaryKey = 'man_number';

    protected $fillable = ['man_number', 'f_name', 'l_name', 'email', 'phone', 'physical_address', 'position'];

    public function debit()
{
    return $this->hasOne('App\Staff_with_debits', 'man_number');
}

    public function credit_receipts()
    {
        return $this->hasMany('App\Credit', 'man_number');
    }

    public function debit_receipts()
    {
        return $this->hasMany('App\Debit', 'man_number');
    }

    public function issuer()
    {
        return $this->hasMany('App\Debit', 'issuer');
    }

    public function received()
    {
        return $this->hasMany('App\Credit', 'paid_to');
    }
}
