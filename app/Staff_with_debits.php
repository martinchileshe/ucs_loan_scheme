<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff_with_debits extends Model
{
    //
    protected $table = 'staff_with_debits';
    protected $fillable = ['amount_debited', 'amount_due','debit_id', 'man_number'];
    protected $primaryKey = 'debit_id';

    public function credit_receipts(){

        return $this->hasMany('App\Credit', 'man_number');
    }

    public function user(){

        return $this->belongsTo('App\Staff', 'man_number');
    }

    public function debit_receipt(){

        return $this->belongsTo('App\Credit', 'debit_id');
    }
}
