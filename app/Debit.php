<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    //
    public $increments = false;
    protected $table = 'debit_receipt';
    protected $fillable = ['man_number', 'amount_debited', 'pec_interest_rate', 'issuer', 'debit_id'];
    protected $primaryKey = 'debit_id';

public function debit()
{
    return $this->hasOne('App\Staff_with_debits');
}

public function owner()
{
    return $this->belongsTo('App\Staff', 'man_number');
}

public function may_generate()
{
    return $this->hasOne('App\Satff_with_debits');
}
}