<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    //protected $timestamps = false;
    protected $table = 'credit_receipt';
    protected $fillable = ['man_number', 'amount_credited', 'paid_to', 'credit_id'];
    public $incrementing = false;

    public function credit()
    {
        return $this->belongsTo('App\Staff', 'man_number');
    }

    public function debit()
    {
        return $this->hasOne('App\Satff_with_debits', 'man_number');
    }

}
